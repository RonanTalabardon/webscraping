import pandas as pd
from pyquery import PyQuery as pq
import requests

# Fonction pour récupérer les données à partir de l'URL donnée
def obtenir_donnees(url):
    response = requests.get(url)
    if response.status_code == 200:
        html = response.text
        return pq(html)
    else:
        raise Exception(f"Erreur lors de la récupération de la page. Code de statut : {response.status_code}")

# URL de la page à scraper
url = "https://www.leportagesalarial.com/coworking/"

# Récupérer les données de la page
page = obtenir_donnees(url)

# Initialiser des listes pour stocker les données
noms_coworking = []
urls_coworking = []

# Parcourir les éléments de la page pour extraire les informations nécessaires
for element in page('h3:contains("Coworking Paris") + ul li'):
    element = pq(element)
    nom = element.find("a").text()
    url_coworking = element.find("a").attr("href")
    
    noms_coworking.append(nom)
    urls_coworking.append(url_coworking)

# Créer un DataFrame pandas avec les données
data = {"Nom": noms_coworking, "URL": urls_coworking}
df = pd.DataFrame(data)

# Enregistrez le DataFrame dans un fichier Excel
df.to_excel("coworking_paris.xlsx", index=False)

print("Les données ont été enregistrées avec succès dans le fichier Excel.")
