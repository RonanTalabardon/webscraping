import pandas as pd
import re

# Fonction pour nettoyer une adresse
def clean_address(address):
    if isinstance(address, float):  # Vérifier si l'adresse est de type float (NaN)
        return "Non disponible"
    address = re.sub(r'Adresse\s*:\s*', '', address, flags=re.IGNORECASE)
    address = re.sub(r'\s*,\s*', ',', address)
    address = re.sub(r'\s+', ' ', address)
    return address.strip()

# Charger le fichier Excel
df = pd.read_excel("coworking_paris_mis_a_jour.xlsx")

# Supprimer les lignes où 'adresse' contient NaN
df = df.dropna(subset=['adresse'])

# Filtrer uniquement les lignes correspondant à Paris
df = df[df['adresse'].str.contains('75', na=False)]

# Nettoyer les adresses
df['adresse'] = df['adresse'].apply(clean_address)

# Nettoyer les autres colonnes si nécessaire
# Exemple: nettoyer les numéros de téléphone
def clean_telephone(telephone):
    if isinstance(telephone, float):  # Vérifier si le téléphone est de type float (NaN)
        return "Non disponible"
    return re.sub(r'\D', '', telephone)  # Garder uniquement les chiffres

df['telephone'] = df['telephone'].apply(clean_telephone)

# Vous pouvez ajouter plus de fonctions de nettoyage ici pour d'autres colonnes si nécessaire

# Enregistrer le DataFrame nettoyé dans un nouveau fichier Excel
df.to_excel("coworking_paris_cleaned.xlsx", index=False)

print("Les données ont été nettoyées et enregistrées avec succès dans le fichier Excel.")