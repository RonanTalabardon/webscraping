from pyquery import PyQuery as pq
import pandas as pd

# Fonction pour extraire les informations
def extraire_informations(lien):
    # Web
    page = pq(url=lien)
    titre = page('.penci-page-header h1').text()

    # Récupérer l'image principale (première image de la page)
    image_principale = page('.inner-post-entry img:first').attr('src')

    # Récupérer tous les paragraphes disponibles dans la zone .inner-post-entry
    paragraphes = page('.inner-post-entry p').text()

    # Si la description est vide, essayons de récupérer du texte à l'intérieur de la balise .post-entry
    if not paragraphes:
        paragraphes = page('.post-entry p').text()

    # Si la description est toujours vide, essayons de récupérer du texte à l'intérieur de la balise .post-entry
    if not paragraphes:
        paragraphes = page('.post-entry').text()

    # Récupérer l'adresse
    adresse_element = page('h2:contains("Contacter") + ul li:contains("Adresse")')
    if not adresse_element:
        # Si l'adresse n'est pas trouvée dans les informations standardes, rechercher dans la description
        adresse_element = page('.inner-post-entry:contains("Adresse")').find('li:contains("Adresse")')
    adresse = adresse_element.text().replace('Adresse :', '').strip() if adresse_element else "Non disponible"

    # Récupérer le téléphone
    telephone = page('h2:contains("Contacter") + ul li:contains("Téléphone")').text().replace('Téléphone :', '').strip()
    telephone = telephone if telephone else "Non disponible"

    # Récupérer l'accès
    acces = page('h2:contains("Contacter") + ul li:contains("Accès")').text().replace('Accès :', '').strip()
    acces = acces if acces else "Non disponible"

    # Récupérer le site
    site = page('h2:contains("Contacter") + ul li:contains("Site") a').attr('href')
    site = site if site and '?' not in site else "Non disponible"

    # Récupérer les liens sociaux
    twitter = page('a[href*="twitter.com"]').attr('href')
    twitter = twitter if twitter and '?' not in twitter else "Non disponible"

    facebook = page('a[href*="facebook.com"]').attr('href')
    facebook = facebook if facebook and '?' not in facebook else "Non disponible"

    linkedin = page('a[href*="linkedin.com"]').attr('href')
    linkedin = linkedin if linkedin and '?' not in linkedin else "Non disponible"

    # Meta
    titremeta = page('title').text()
    descriptionmeta = page('meta[property="og:description"]').attr('content')

    # Vérifier la longueur du meta title
    longueur_meta_title = len(titremeta)
    meta_title_inferieur_a_150 = longueur_meta_title < 150

    # Retourner les informations sous forme de dictionnaire
    return {
        'Nom': titre,
        'URL': lien,
        'image_principale': image_principale,
        'description': paragraphes,
        'adresse': adresse,
        'telephone': telephone,
        'acces': acces,
        'site': site,
        'twitter': twitter,
        'facebook': facebook,
        'linkedin': linkedin,
        'titremeta': titremeta,
        'descriptionmeta': descriptionmeta,
        'meta_title_inferieur_a_150': meta_title_inferieur_a_150
    }

# Lire le fichier Excel
df_lu = pd.read_excel("coworking_paris.xlsx")

# Filtrer les liens contenant "75" (Paris)
liens_paris = df_lu[df_lu['URL'].str.contains('75')]['URL'].tolist()

# Ajouter des colonnes pour les informations
colonnes_informations = ['Nom', 'URL', 'image_principale', 'description', 'adresse', 'telephone',
                         'acces', 'site', 'twitter', 'facebook', 'linkedin',
                         'titremeta', 'descriptionmeta', 'meta_title_inferieur_a_150']

for colonne in colonnes_informations:
    df_lu[colonne] = ""

# Utiliser les liens pour extraire les informations et mettre à jour le DataFrame
for i, lien in enumerate(liens_paris):
    informations = extraire_informations(lien)
    for colonne, valeur in informations.items():
        df_lu.at[i, colonne] = valeur

# Enregistrer le DataFrame mis à jour dans un nouveau fichier Excel
df_lu.to_excel("coworking_paris_mis_a_jour.xlsx", index=False)

print("Les informations de Paris ont été ajoutées avec succès au fichier Excel.")